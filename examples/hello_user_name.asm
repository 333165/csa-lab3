section data:
    buf: 'What is your name?',10,0
    buf1: 'Hello, ',0
    input_buffer: 0,0,0,0,0,0,0
    input: 0
    output: 1
section text:
    _start:
        lw r5, input
        lw r6, output
        add r1, r0, buf
    loop1:
        lw r3, r1
        beq r3, r0, break1
        sw r6, r3
        add r1, r1, 1
        jmp loop1
    break1:
        add r1, r0, input_buffer
    loop3:
        lw r3, r5
        sw r1, r3
        beq r3, r0, break3
        add r1, r1, 1
        jmp loop3
    break3:
        add r1, r0, buf1
    loop2:
        lw r3, r1
        beq r3, r0, break2
        sw r6, r3
        add r1, r1, 1
        jmp loop2
    break2:
        add r1, r0, input_buffer
    loop4:
        lw r3, r1
        beq r3, r0, break4
        sw r6, r3
        add r1, r1, 1
        jmp loop4
    break4:
        halt