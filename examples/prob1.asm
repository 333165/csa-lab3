section data:
sum: 0
current_number: 1000
result: 1
output: 1
output_buffer: 0,0,0,0,0,0,0

section text:
    _start:
        lw r1,current_number
        lw r5, output
    loop:
        sub r1,r1,1
        beq r1, r0, break
        rem r3,r1,3
        bne r0,r3,next
        add r4,r4,r1
        jmp loop
    next:
        rem r3,r1,5
        bne r0,r3,loop
        add r4,r4,r1
        jmp loop
    break:
        add r3, r0, output_buffer
    loop1:
        rem r2, r4, 10
        add r2, r2, 48
        sw r3, r2
        div r4, r4, 10
        beq r4, r0, break1
        add r3, r3, 1
        jmp loop1
    break1:
        add r1, r0, output_buffer
    loop2:
        blt r3, r1, end
        lw r2, r3
        sw r5, r2
        sub r3, r3, 1
        jmp loop2
    end:
        halt