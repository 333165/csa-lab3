section data:
    n: 9
    output_buffer: 0,0,0,0,0,0,0
    output: 1
section text:
    _start:
        lw r1, n            ; input
        beq r1, r0, break     ; if == 0
        add r6, r0, 1       ; factorial
        add r3, r0, 1       ; counter
        add r4, r1, 1       ; n + 1
    loop:
        mul r6, r6, r3
        add r3, r3, 1
        beq r3, r4, break
        jmp loop
    break:
        add r3, r0, output_buffer
        lw r5, output
    loop1:
        rem r2, r6, 10
        add r2, r2, 48
        sw r3, r2
        div r6, r6, 10
        beq r6, r0, break1
        add r3, r3, 1
        jmp loop1
    break1:
        add r1, r0, output_buffer
    loop2:
        blt r3, r1, end
        lw r2, r3
        sw r5, r2
        sub r3, r3, 1
        jmp loop2
    end:
        halt