section data:
    buf: 'Hello, World!',0
    output: 1

section text:
    _start:
        lw r1, output
        add r2, r0, buf
    loop:
        lw r3, r2
        beq r3, r0, end
        sw r1, r3
        add r2, r2, 1
        jmp loop
    end:
        halt
