section data:
    buf: 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    input: 0
    output: 1

section text:
    _start:
        add r4, r0, buf
        lw r5, input
        lw r6, output
    loop1:
        lw r1, r5
        beq r1, r0, break1
        sw r4, r1
        add r4, r4, 1
        jmp loop1
    break1:
        add r4, r0, buf
    loop2:
        lw r1, r4
        beq r1, r0, end
        sw r6, r1
        add r4, r4, 1
        jmp loop2
    end:
        halt
